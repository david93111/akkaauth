package co.com.auth

import co.com.auth.conf.AppConf
import com.typesafe.config.Config
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

trait BaseSimulation extends Simulation{

  val config: Config = AppConf.conf
  val baseUrl: String

  lazy val httpProtocol: HttpProtocolBuilder = http
    .baseUrl(baseUrl) // Root of all relative URLs on simulations
    .acceptHeader("application/json,text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .header("Content-Type", "application/json")
    .userAgentHeader("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")

}
