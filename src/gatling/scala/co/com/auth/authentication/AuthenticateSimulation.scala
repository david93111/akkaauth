package co.com.auth.authentication

import co.com.auth.BaseSimulation
import io.gatling.core.Predef._
import io.gatling.core.controller.inject.open.OpenInjectionStep
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._

import scala.concurrent.duration._

class AuthenticateSimulation extends BaseSimulation{

  val baseUrl: String = config.getString("gatling.baseURL")

  val scenarioBuilder : (String, String, Int) => ScenarioBuilder = (name, body, expectedStatus) =>
    scenario(name).
      exec(
        http("AuthenticateUser")
          .post("/akka-auth/authenticate")
          .body(StringBody(body))
          .check(status is expectedStatus)
      )

  def buildBody(user: String, pass: String):String ={
    s"""
      |{
      |	"username":"$user",
      |	"password":"$pass"
      |}
    """.stripMargin
  }

  val successBody: String = buildBody("test", "TEST")

  val invalidCredentialsBody: String = buildBody("test", "test")

  val failureBody: String = buildBody("Atest", "ATEST")

  val successAuthScenario: ScenarioBuilder = scenarioBuilder("SuccessToken", successBody, 200)

  val invalidCredentialsScenario: ScenarioBuilder = scenarioBuilder("InvalidCredentials", invalidCredentialsBody, 401)

  val failureScenario: ScenarioBuilder = scenarioBuilder("FailTokenGeneration", failureBody, 502)

  val rampUpUsersCommonInjection: OpenInjectionStep = rampUsers(5000).during(FiniteDuration(5, MINUTES))

  setUp(
    successAuthScenario.inject(rampUpUsersCommonInjection),
    invalidCredentialsScenario.inject(rampUpUsersCommonInjection),
    failureScenario.inject(rampUpUsersCommonInjection),
  ).protocols(httpProtocol)

}
