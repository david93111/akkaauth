package co.com.auth.boot

import akka.actor.ActorSystem
import akka.event.{Logging, LoggingAdapter}
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import co.com.auth.api.Api
import co.com.auth.conf.AppConf.{appHost, appPort}
import scala.concurrent.ExecutionContextExecutor
import scala.util.{Failure, Success}

/** App Entry point/ startup
  *
  * Starts and handle bingind of Akka-HTTP server
  *
  * */
object Boot extends App{

  implicit val actorSystem: ActorSystem = ActorSystem("akka-auth-system")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  // needed for the future flatMap/onComplete in the end
  implicit val executionContext: ExecutionContextExecutor = actorSystem.dispatcher

  val log: LoggingAdapter = Logging(actorSystem.eventStream, "akka_auth_log")

  val api = new Api(log)(actorSystem)
  val bindingFuture = Http().bindAndHandle(api.route, appHost , appPort)

  bindingFuture.onComplete{
    case Success(value) =>
      log.info(s"Binding Successfully achieved, server started on $appHost:$appPort")
      actorSystem.registerOnTermination(value.unbind())
    case Failure(exception) =>
      log.error(s"HTTP server binding Failed, starting actor system shutdown, cause: $exception")
      actorSystem.terminate()
  }

}
