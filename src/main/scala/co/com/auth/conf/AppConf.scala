package co.com.auth.conf

import com.typesafe.config.{Config, ConfigFactory}

import scala.collection.mutable
import scala.collection.JavaConverters._
import scala.concurrent.duration.Duration

/**
  * Factory for application configuration and exposure of general configuration values on Application.conf.
  * */
object AppConf {

  val conf: Config = ConfigFactory.load("application")

  val serverConf: Config = conf.getConfig("http")
  val appHost: String = serverConf.getString("host")
  val appPort: Int = serverConf.getInt("port")

  val appConfig: Config = conf.getConfig("app")

  val allowedOrigins: mutable.Seq[String] = conf.getStringList("cors.allowed-origins").asScala

  val defaultAskTimeoutActors: Duration = Duration(appConfig.getString("default.ask-seconds-timeout"))

}
