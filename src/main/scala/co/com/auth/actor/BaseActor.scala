package co.com.auth.actor

import akka.actor.Actor
import akka.event.Logging
import co.com.auth.actor.util.BaseTimeout

trait BaseActor extends Actor with BaseTimeout{
  val logger = Logging(context.system, this)
}
