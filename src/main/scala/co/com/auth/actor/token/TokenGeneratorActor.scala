package co.com.auth.actor.token

import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

import akka.actor.Props
import akka.pattern.after
import co.com.auth.actor.BaseActor
import co.com.auth.actor.token.TokenGeneratorActor.GenerateToken
import co.com.auth.model.error.ServiceError
import co.com.auth.model.error.exception.TechnicalFailure
import co.com.auth.model.{User, UserToken}

import scala.concurrent.duration.{FiniteDuration, MILLISECONDS}
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Random

/** Token Generator actor as router for parallel processing of authentication requests */
class TokenGeneratorActor()(implicit val ex: ExecutionContext) extends BaseActor {

  val randomGenerator = new Random()
  val dateFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ")

  override def receive: Receive = {

    case GenerateToken(user) =>
      val millisecondsDelay = randomGenerator.nextInt(5000)
      val randomDuration = FiniteDuration(millisecondsDelay,MILLISECONDS)
      val result = after(randomDuration, context.system.scheduler)(generateToken(user.userId))
      sender ! result
    case _ => // Added instruction to avoid dead letters quiet handling
      sender ! Future.failed(ServiceError.unexpectedMessage)
  }

  def generateToken(userId: String): Future[UserToken] = {
    logger.info("Processing token generation for user {}", userId)
    if (userId.startsWith("A"))
      Future.failed(TechnicalFailure("The User can be processed if starts with 'A'"))
    else Future{
      val now = ZonedDateTime.now()
      val formattedTime = now.format(dateFormatter)
      UserToken(s"${userId}_$formattedTime")
    }
  }

}

object TokenGeneratorActor{

  case class GenerateToken(user: User)

  def props()(implicit ex: ExecutionContext) : Props = {
    Props(new TokenGeneratorActor())
  }

}
