package co.com.auth.actor.auth

import akka.actor.{Props, Scheduler}
import akka.pattern.after
import co.com.auth.actor.BaseActor
import co.com.auth.actor.auth.AuthActor.AuthenticateUser
import co.com.auth.model.error.ServiceError
import co.com.auth.model.error.exception.InvalidCredentials
import co.com.auth.model.{Credentials, User}

import scala.concurrent.duration.{FiniteDuration, MILLISECONDS}
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Random

/** Authentication actor as router for parallel processing of authentication requests */
class AuthActor()(implicit val ec: ExecutionContext) extends BaseActor {

  val randomGenerator = new Random()

  val scheduler: Scheduler = context.system.scheduler

  override def receive: Receive = {
    case AuthenticateUser(credentials) =>
      val millisecondsDelay = randomGenerator.nextInt(5000)
      val randomDuration = FiniteDuration(millisecondsDelay,MILLISECONDS)
      val result: Future[User] = after(randomDuration, scheduler)(authenticateUser(credentials.username, credentials.password))
      sender ! result
    case _ => // Added instruction to avoid dead letters quiet handling
      sender() ! Future.failed(ServiceError.unexpectedMessage)
  }

  def authenticateUser(user: String, pass: String): Future[User] = {
    logger.info("Processing authentication for user {}", user)
      if(pass == user.toUpperCase())
        Future.successful(User(user))
      else
        Future.failed(InvalidCredentials(s"Invalid credentials for provided username: $user"))
  }

}

object AuthActor{

  case class AuthenticateUser(credentials: Credentials)

  def props()(implicit ex: ExecutionContext) : Props = {
    Props(new AuthActor())
  }

}
