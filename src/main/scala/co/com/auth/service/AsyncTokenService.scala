package co.com.auth.service

import co.com.auth.model.{Credentials, User, UserToken}

import scala.concurrent.{ExecutionContext, Future}

trait AsyncTokenService {

  protected def authenticate(credentials: Credentials): Future[User]
  protected def issueToken(user: User): Future[UserToken]

  def requestToken(credentials: Credentials)(implicit execContext: ExecutionContext): Future[UserToken] = {
    for {
      validUser <- authenticate(credentials)
      token <- issueToken(validUser)
    } yield token
  }

}
