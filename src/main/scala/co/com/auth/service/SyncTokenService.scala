package co.com.auth.service

import co.com.auth.model.{Credentials, User, UserToken}

trait SyncTokenService{

  protected def authenticate(credentials: Credentials): User
  protected def issueToken(user: User): UserToken

  def requestToken(credentials: Credentials): UserToken = {
    val validUser = authenticate(credentials)
    issueToken(validUser)
  }

}
