package co.com.auth.api.service
import akka.actor.ActorRef
import akka.dispatch.MessageDispatcher
import akka.event.LoggingAdapter
import akka.pattern.ask
import co.com.auth.actor.auth.AuthActor
import co.com.auth.actor.token.TokenGeneratorActor
import co.com.auth.actor.util.BaseTimeout
import co.com.auth.model.{Credentials, User, UserToken}

import scala.concurrent.Future

trait ApiServices extends SimpleAsyncTokenService with BaseTimeout{

  val log: LoggingAdapter

  implicit val executionContext: MessageDispatcher

  val authActorRouter: ActorRef

  val tokenActorRouter: ActorRef

  override def requestToken(credentials: Credentials): Future[UserToken] = {
    val authUserResult = authActorRouter ? AuthActor.AuthenticateUser(credentials)
    log.info("Called auth actor")
    for {
      userFuture <- authUserResult.mapTo[Future[User]]
      user <- userFuture
      tokenFuture <- (tokenActorRouter ? TokenGeneratorActor.GenerateToken(user)).mapTo[Future[UserToken]]
      token <- tokenFuture
    } yield token
  }

}
