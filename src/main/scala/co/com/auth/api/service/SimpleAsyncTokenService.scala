package co.com.auth.api.service

import co.com.auth.model.{Credentials, UserToken}

import scala.concurrent.Future

trait SimpleAsyncTokenService {
  def requestToken(credentials: Credentials): Future[UserToken]
}
