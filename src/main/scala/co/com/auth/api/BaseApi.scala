package co.com.auth.api

import akka.http.scaladsl.model.DateTime
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.Directives._
import co.com.auth.api.handler.BaseHandlers

/**  Api technical services
  *
  *  @author david93111
  *
  *  includes handlers for Rejections and Exceptions, also management of Cross Site
  *
  *  Expose service of health check for deployment and balancing
  *  Expose service of version to be able to see deployment version and verify if compatible with client library
  *
  * */
trait BaseApi extends BaseHandlers{

  def healthCheck(): Route = {
    (path("health_check") & get){
      complete(OK -> s"Akka Auth OK - ${DateTime.now.toString()}")
    }
  }

  /** BuildInfo object generated with sbt plugin to access current sbt version in a transparent way */
  def version(): Route = {
    (path("version") & get){
      complete(OK -> s"v${BuildInfo.version}")
    }
  }

}
