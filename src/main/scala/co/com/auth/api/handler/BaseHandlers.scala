package co.com.auth.api.handler

import akka.event.LoggingAdapter
import co.com.auth.api.handler.crossorigin.CrossOriginHandler
import co.com.auth.api.handler.exception.CustomExceptionHandler
import co.com.auth.api.handler.rejection.CustomRejectionHandler

/** central definition of handlers for Rejections, Exceptions and also management of Cross Site */
trait BaseHandlers {

  val log: LoggingAdapter

  val crossOriginHandler = new CrossOriginHandler
  val exceptionHandler = new CustomExceptionHandler(log)
  val rejectionHandler = new CustomRejectionHandler(log)

}
