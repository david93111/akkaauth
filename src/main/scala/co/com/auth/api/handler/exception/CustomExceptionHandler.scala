package co.com.auth.api.handler.exception

import akka.event.LoggingAdapter
import akka.http.scaladsl.model.StatusCodes.{BadRequest, InternalServerError, Unauthorized, BadGateway}
import akka.http.scaladsl.server.Directives.{complete, handleExceptions}
import akka.http.scaladsl.server.{Directive0, ExceptionHandler}
import co.com.auth.model.error.exception.{InvalidCredentials, TechnicalFailure}
import co.com.auth.model.error.{ErrorCause, ServiceError}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

/** Custom Akka-HTTP Handler for  exception*/
class CustomExceptionHandler(log: LoggingAdapter) {

  val handler: ExceptionHandler = ExceptionHandler {
    case serviceError: ServiceError =>
      log.error("Handled error ocurred {}", serviceError)
      complete(InternalServerError -> serviceError)
    case invalidCredentials: InvalidCredentials =>
      log.error("Invalid Credentials Error ocurred {}", invalidCredentials)
      complete(Unauthorized -> ServiceError.invalidCredentials(invalidCredentials.message))
    case technicalFailure: TechnicalFailure =>
      log.error("Technical Error ocurred {}", technicalFailure)
      complete(BadGateway -> ServiceError.technicalFailure(technicalFailure.message))
    case illegalArgument: java.lang.IllegalArgumentException =>
      log.error("Invalid request made {}", illegalArgument)
      complete(BadRequest -> ServiceError.invalidParameters(illegalArgument.getMessage))
    case e: Throwable =>
      log.error("Unexpected and unhandled error, cause: {}", e)
      complete(InternalServerError -> ServiceError(ErrorCause.UnexpectedServerError, s"Error Cause: ${e.getMessage}"))
  }

  def exceptionHandler: Directive0 = handleExceptions(handler)

}
