package co.com.auth.api.handler.rejection

import akka.event.LoggingAdapter
import akka.http.scaladsl.model.StatusCodes.BadRequest
import akka.http.scaladsl.server.Directives.{complete, handleRejections}
import akka.http.scaladsl.server.{Directive0, MalformedRequestContentRejection, RejectionHandler, ValidationRejection}
import co.com.auth.model.error.ServiceError
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

/** Custom Akka-HTTP Handler for api rejections */
class CustomRejectionHandler(log: LoggingAdapter) {
  val handler: RejectionHandler = RejectionHandler.newBuilder().handle({
    case ValidationRejection(msg, _) =>
      log.warning("Request Rejection: request does not meet the validations ")
      complete(BadRequest -> ServiceError.invalidParameters(msg))
    case MalformedRequestContentRejection(msg,_) =>
      log.warning("Request Rejection: request payload was invalid ")
      complete(BadRequest -> ServiceError.invalidPayload(msg))
  }).result().withFallback(RejectionHandler.default)

  def rejectionHandler: Directive0 = handleRejections(handler)
}
