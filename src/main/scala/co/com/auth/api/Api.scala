package co.com.auth.api

import akka.actor.{ActorRef, ActorSystem}
import akka.dispatch.MessageDispatcher
import akka.event.LoggingAdapter
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.routing.FromConfig
import co.com.auth.actor.auth.AuthActor
import co.com.auth.actor.token.TokenGeneratorActor
import co.com.auth.api.service.ApiServices
import co.com.auth.model.Credentials
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

/**  RestFul Api Route for Game Services
  *
  *  @author david93111
  *
  *  Expose service of authentication, health check and version
  *
  *  services layer on [[co.com.auth.api.service.ApiServices]] is on charge of sending messages to Actor Router
  *
  *  Exceptions and Rejections are managed using default custom Handlers provided by [[co.com.auth.api.BaseApi]]
  *
  * */
class Api(val log: LoggingAdapter)(implicit val actorSystem: ActorSystem) extends BaseApi with ApiServices {

  implicit val executionContext: MessageDispatcher = actorSystem.dispatchers.lookup("dispatchers.base-dispatcher")

  /**No supervision strategy required as the actor failures are handled directly as futures and in this case actors
    * are ironically stateless
    **/
  val authActorRouter: ActorRef = actorSystem.actorOf(FromConfig.props(AuthActor.props()(executionContext)), "authenticationRouter")

  val tokenActorRouter: ActorRef =  actorSystem.actorOf(FromConfig.props(TokenGeneratorActor.props()(executionContext)), "tokenRouter")

  val route: Route  = exceptionHandler.exceptionHandler {
    rejectionHandler.rejectionHandler {
      pathPrefix("akka-auth") {
        crossOriginHandler.handler {
          path("authenticate") {
            post{
              entity(as[Credentials]){ credentials =>
                log.info("Starting Authentication for user {}", credentials.username)
                onSuccess(requestToken(credentials)){ result =>
                  log.info("Token {} generated for user {}", result.token, credentials.username)
                  complete(OK, result)
                }
              }
            }
          } ~ version() ~ healthCheck()
        }
      }
    }
  }
}
