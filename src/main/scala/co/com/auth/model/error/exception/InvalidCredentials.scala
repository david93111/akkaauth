package co.com.auth.model.error.exception

import scala.util.control.NoStackTrace

case class InvalidCredentials(message: String) extends Exception(message) with NoStackTrace
