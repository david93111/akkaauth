package co.com.auth.model.error

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto._

import scala.util.control.NoStackTrace

/** General error representation for marshalling */
case class ServiceError(cause: ErrorCause, message: String) extends Exception(message) with NoStackTrace

object ServiceError{

  implicit val serviceErrorDecoder: Decoder[ServiceError] = deriveDecoder[ServiceError]
  implicit val serviceErrorEncoder: Encoder[ServiceError] = deriveEncoder[ServiceError]

  def invalidCredentials(message: String): ServiceError = ServiceError(ErrorCause.InvalidCredentials, message)

  def technicalFailure(message: String): ServiceError = ServiceError(ErrorCause.TechnicalFailure, message)

  def invalidParameters(message: String): ServiceError = ServiceError(ErrorCause.InvalidParameters, message)

  def invalidPayload(message: String): ServiceError = ServiceError(ErrorCause.InvalidPayload, message)

  def unexpectedMessage: ServiceError =
    ServiceError(ErrorCause.UnexpectedMessage, "Error processing your request, internal error sending the message")

}
