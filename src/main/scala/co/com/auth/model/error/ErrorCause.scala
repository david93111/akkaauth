package co.com.auth.model.error

import enumeratum._

import scala.collection.immutable.IndexedSeq

sealed trait ErrorCause extends EnumEntry
case object ErrorCause extends Enum[ErrorCause] with CirceEnum[ErrorCause] {

  case object InvalidCredentials extends ErrorCause
  case object UnexpectedMessage extends ErrorCause
  case object InvalidParameters extends ErrorCause
  case object UnexpectedServerError extends ErrorCause
  case object InvalidPayload extends ErrorCause
  case object TechnicalFailure extends ErrorCause

  val values: IndexedSeq[ErrorCause] = findValues

}
