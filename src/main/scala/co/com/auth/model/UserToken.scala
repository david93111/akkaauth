package co.com.auth.model

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto._

case class UserToken(token: String)

object UserToken{
  implicit val userTokenDecoder: Decoder[UserToken] = deriveDecoder[UserToken]
  implicit val userTokenEncoder: Encoder[UserToken] = deriveEncoder[UserToken]
}
