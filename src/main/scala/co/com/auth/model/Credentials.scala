package co.com.auth.model

import io.circe.generic.semiauto._
import io.circe.{Decoder, Encoder}

case class Credentials(username: String, password: String){
  require(username.nonEmpty, "username is mandatory, must not be empty nor null")
  require(password.nonEmpty, "password is mandatory,must not be empty nor null")
}

object Credentials{

  implicit val credentialsDecoder: Decoder[Credentials] = deriveDecoder[Credentials]
  implicit val credentialsEncoder: Encoder[Credentials] = deriveEncoder[Credentials]

}
