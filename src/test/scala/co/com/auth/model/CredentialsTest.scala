package co.com.auth.model

import co.com.auth.BaseTest

import scala.util.Try

class CredentialsTest extends BaseTest{

  "Credentials creation " should {

    "Fail if username is an empty string" in {
      Try(Credentials("", "pass"))
        .failed
        .map(ex => ex shouldBe classOf[IllegalArgumentException])
    }

    "Fail if password is an empty string" in {
      Try(Credentials("user", ""))
        .failed
        .map(ex => ex shouldBe classOf[IllegalArgumentException])
    }

    "Fail if user is null" in {
      Try(Credentials(null, "pass"))
        .failed
        .map(ex => ex shouldBe classOf[IllegalArgumentException])
    }

    "Fail if password is null" in {
      Try(Credentials("user", null))
        .failed
        .map(ex => ex shouldBe classOf[IllegalArgumentException])
    }

  }

}
