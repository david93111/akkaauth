package co.com.auth

import org.scalatest.{Matchers, WordSpecLike}

trait BaseTest extends WordSpecLike with Matchers
