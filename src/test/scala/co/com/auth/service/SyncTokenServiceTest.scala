package co.com.auth.service
import co.com.auth.model.{Credentials, User, UserToken}
import org.scalatest.WordSpecLike

class SyncTokenServiceTest extends BaseTokenServiceTest with WordSpecLike{

  class SyncTokenServiceImpl extends SyncTokenService {
    override protected def authenticate(credentials: Credentials): User = User(credentials.username)

    override protected def issueToken(user: User): UserToken = UserToken(s"${user.userId}_$token_date")
  }

  val asyncService = new SyncTokenServiceImpl

  "Sync Token Service implement" should {

    "Generate token if credentials are valid" in {
      val credentials = Credentials("user", "USER")
      val token = asyncService.requestToken(credentials)
      token.token should startWith(credentials.username)
    }

  }

}
