package co.com.auth.service

import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

import org.scalatest.Matchers

trait BaseTokenServiceTest extends Matchers{

  private val dateFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ")

  val token_date: String = ZonedDateTime.now().format(dateFormatter)

}
