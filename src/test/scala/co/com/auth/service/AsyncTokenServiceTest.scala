package co.com.auth.service

import co.com.auth.model.{Credentials, User, UserToken}
import org.scalatest.AsyncWordSpecLike

import scala.concurrent.Future

class AsyncTokenServiceTest extends BaseTokenServiceTest with AsyncWordSpecLike {


  class AsyncTokenServiceImpl extends AsyncTokenService {
    override protected def authenticate(credentials: Credentials): Future[User] = {
      Future.successful(User(credentials.username))
    }

    override protected def issueToken(user: User): Future[UserToken] = {
      Future.successful(UserToken(s"${user.userId}_$token_date"))
    }
  }

  val asyncService = new AsyncTokenServiceImpl

  "Async Token Service implement" should {

    "Generate token if credentials are valid" in {
        val credentials = Credentials("user", "USER")
        val token = asyncService.requestToken(credentials)
        token.map(token => assert(token.token.startsWith(credentials.username)))
    }

  }

}
