package co.com.auth.api

import akka.event.LoggingAdapter
import akka.http.scaladsl.model.{ContentTypes, StatusCodes}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.{RouteTestTimeout, ScalatestRouteTest}
import akka.testkit.TestDuration
import co.com.auth.BaseTest
import co.com.auth.model.error.{ErrorCause, ServiceError}
import co.com.auth.model.{Credentials, UserToken}

import scala.concurrent.duration._


class ApiTest extends BaseTest with ScalatestRouteTest{

  implicit val timeout: RouteTestTimeout = RouteTestTimeout(10.seconds.dilated)

  val log: LoggingAdapter = system.log

  val api: Api = new Api(log)(system)

  val route: Route = api.route

  "Api authenticate user service" should {
    "Respond with 200 OK and a valid token when authentication requested with valid credentials" in {
      import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
      val credentials = Credentials("user", "USER")
      Post("/akka-auth/authenticate",credentials) ~> route ~> check {
        status shouldEqual StatusCodes.OK
        contentType shouldEqual ContentTypes.`application/json`
        responseAs[UserToken].token should startWith(credentials.username)
      }
    }

    "Respond with 401 Unauthorized when authentication requested with invalid credentials" in {
      import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
      val credentials = Credentials("user", "user")
      Post("/akka-auth/authenticate",credentials) ~> route ~> check {
        status shouldEqual StatusCodes.Unauthorized
        contentType shouldEqual ContentTypes.`application/json`
        responseAs[ServiceError].cause shouldBe ErrorCause.InvalidCredentials
      }
    }

    "Respond with 502 BadGateway when authentication fail due to a technical issue" in {
      import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
      val credentials = Credentials("Auser", "AUSER")
      Post("/akka-auth/authenticate",credentials) ~> route ~> check {
        status shouldEqual StatusCodes.BadGateway
        contentType shouldEqual ContentTypes.`application/json`
        responseAs[ServiceError].cause shouldBe ErrorCause.TechnicalFailure
      }
    }

    "Respond with 400 BadRequest when credentials do not fit minimum requirements" in {
      val body =
        """
          |{
          |	"username": "",
          |	"password": "TEST"
          |}
        """.stripMargin

      Post("/akka-auth/authenticate").withEntity(ContentTypes.`application/json`, body) ~> route ~> check {
        import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
        status shouldEqual StatusCodes.BadRequest
        contentType shouldEqual ContentTypes.`application/json`
        responseAs[ServiceError].cause shouldBe ErrorCause.InvalidParameters
      }
    }

    "Respond with 415 MethodNotAllowed when using invalid HTTP Method" in {
      Get("/akka-auth/authenticate") ~> route ~> check {
        status shouldEqual StatusCodes.MethodNotAllowed
      }
    }

    "Respond with 400 BadRequest when Payload is invalid" in {
      import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
      Post("/akka-auth/authenticate", "") ~> route ~> check {
        status shouldEqual StatusCodes.BadRequest
        contentType shouldEqual ContentTypes.`application/json`
        responseAs[ServiceError].cause shouldBe ErrorCause.InvalidPayload
      }
    }
  }

  "Api health check service" should {

    "Respond with 200 OK when health_check requested" in {
      Get("/akka-auth/health_check") ~> route ~> check {
        status shouldEqual StatusCodes.OK
        responseAs[String] should startWith("Akka Auth OK")
      }
    }

    "Respond with 405 MethodNotAllowed when health_check requested with wrong method" in {
      Post("/akka-auth/health_check") ~> route ~> check {
        status shouldEqual StatusCodes.MethodNotAllowed
      }
    }

  }

  "Api version service" should {

    "Respond with 200 OK when version requested and version must match" in {
      Get("/akka-auth/version") ~> route ~> check {
        status shouldEqual StatusCodes.OK
        responseAs[String] shouldEqual s"v${BuildInfo.version}"
      }
    }

    "Respond with 405 MethodNotAllowed when version requested and version must match" in {
      Post("/akka-auth/version") ~> route ~> check {
        status shouldEqual StatusCodes.MethodNotAllowed
      }
    }

  }

  "Api Handlers" should{

    "CrossSite handler response with 405 MethodNotAllowed when a non existing service is consumed " in {
      Get("/akka-auth/notExistingService") ~> route ~> check {
        status shouldEqual StatusCodes.MethodNotAllowed
      }
    }

    "Respond with 404 NotFound when services are consumed outside the base path" in {
      Get("/notExistingPath") ~> route ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

  }



}
