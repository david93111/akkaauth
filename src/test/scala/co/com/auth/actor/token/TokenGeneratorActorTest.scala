package co.com.auth.actor.token

import akka.actor.ActorRef
import akka.routing.FromConfig
import co.com.auth.actor.BaseActorTest
import co.com.auth.actor.token.TokenGeneratorActor.GenerateToken
import co.com.auth.model.error.{ErrorCause, ServiceError}
import co.com.auth.model.error.exception.{InvalidCredentials, TechnicalFailure}
import co.com.auth.model.{Credentials, User, UserToken}

import scala.concurrent.Future

class TokenGeneratorActorTest extends BaseActorTest("TokenGeneratorTestSystem") {

  val tokenActor: ActorRef = system.actorOf(FromConfig.props(TokenGeneratorActor.props()(ex)), "authenticationRouter")

  "Authentication Actor Test" should {

    "Give a UserToken instance when user is valid" in {
      val user = User("user")
      tokenActor ! GenerateToken(user)
      expectMsgPF(){
        case token: Future[UserToken] =>
          token.map(tk => assert(tk.token.startsWith(user.userId)))
        case _ =>
          fail("Message of an unexpected type")
      }
    }

    "Give a TechnicalFailuer error when user starts with 'A'" in {
      val user = User("Auser")
      tokenActor ! GenerateToken(user)
      expectMsgPF(){
        case failure: Future[UserToken] =>
          failure.failed.map{
            case _: TechnicalFailure =>
              assert(true)
            case _ =>
              fail("The thrown exception is not the expected one")
          }
        case e =>
          fail(s"Message of an unexpected type ${e.getClass.getName}")
      }
    }

    "Give a ServiceError with UnexpectedMessage cause when message type is not expected by actor" in {
      val user = User("user")
      tokenActor ! user
      expectMsgPF(){
        case failure: Future[Any] =>
          failure.failed.map{
            case error: ServiceError =>
              assert(error.cause == ErrorCause.UnexpectedMessage)
            case _ =>
              fail("The thrown exception is not the expected one")
          }
        case e =>
          fail(s"Message of an unexpected type: ${e.getClass.getName}")
      }
    }

  }


}
