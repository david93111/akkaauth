package co.com.auth.actor.auth

import akka.actor.ActorRef
import akka.routing.FromConfig
import co.com.auth.actor.BaseActorTest
import co.com.auth.actor.auth.AuthActor.AuthenticateUser
import co.com.auth.model.error.exception.InvalidCredentials
import co.com.auth.model.error.{ErrorCause, ServiceError}
import co.com.auth.model.{Credentials, User}

import scala.concurrent.Future

class AuthActorTest extends BaseActorTest("AuthActorTestSystem") {


  val authActor: ActorRef = system.actorOf(FromConfig.props(AuthActor.props()(ex)), "authenticationRouter")

  "Authentication Actor Test" should {

    "Give an user instance when credentials are valid" in {
      val credentials = Credentials("test","TEST")
      authActor ! AuthenticateUser(credentials)
      expectMsgPF(){
        case user: Future[User] =>
          user.map(us => assert(us.userId.equals(credentials.username)))
        case _ =>
          fail("Message of an unexpected type")
      }
    }

    "Give an InvalidCredentials error when credentials are invalid" in {
      val credentials = Credentials("test","test")
      authActor ! AuthenticateUser(credentials)
      expectMsgPF(){
        case failure: Future[User] =>
          failure.failed.map{
            case _: InvalidCredentials =>
              assert(true)
            case _ =>
              fail("The thrown exception is not the expected one")
          }
        case e =>
          fail(s"Message of an unexpected type ${e.getClass.getName}")
      }
    }

    "Give a ServiceError with UnexpectedMessage cause when message type is not expected by actor" in {
      val credentials = Credentials("test","test")
      authActor ! credentials
      expectMsgPF(){
        case failure: Future[Any] =>
          failure.failed.map{
            case error: ServiceError =>
              assert(error.cause == ErrorCause.UnexpectedMessage)
            case _ =>
              fail("The thrown exception is not the expected one")
          }
        case e =>
          fail(s"Message of an unexpected type: ${e.getClass.getName}")
      }
    }

  }

}
