package co.com.auth.actor

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit}
import org.scalatest.{AsyncWordSpecLike, BeforeAndAfterAll, Matchers}

import scala.concurrent.ExecutionContext

abstract class BaseActorTest(sysName: String) extends TestKit(ActorSystem(sysName)) with ImplicitSender with AsyncWordSpecLike
with BeforeAndAfterAll with Matchers{

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  implicit lazy val ex: ExecutionContext= system.dispatcher

}
