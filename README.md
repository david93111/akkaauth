# Akka Auth Microservice

Authentication Micro-service using Akka HTTP and Akka actor

### Considerations and Decisions
The stack used was the one suggested as is great for concurrency and also to manage and isolate state
(but on this case management of state is not needed), using routing actor strategies allow to maximize
parallel processing capabilities, complemented with the Akka Actor After pattern allow to build non blocking random
delayed operations in a transparent way using simple scheduling over futures. 

Akka HTTP plus the Circe (One of the best json libraries for scala) Akka HTTP complement to marshall/un-marshall 
entities allow to build a typed API and brings more readability, testability and maintainability with auto or semi
derivation codecs even for complex entities.

Gatling was added in order to use simple load test to have real behavior metrics over the implemented
solution, this test are really easy to maintain and execute, cause they live inside the component and only
a single command is required to execute this tests, with no further major configurations. 

### Built With

* Akka HTTP
* Akka Actors
* Circe with Akka HTTP Circe for Marshalling of objects to achieve typed API 
* TypeSafe Config for parametrized configuration on component
* Gatling Sbt Plugin for Basic load testing over HTTP API
* ScalaTest + AkkaTestKit + RouteScalaTest for Unit Testing
* Sbt Coverage and Sbt Native Package Plugins for test, coverage and packaging

__*NOTE:*__ The project has the Sbt Dependecy graph plugin to see the dependencies tree or graph in order
to know current version and have clear vision over compatibility, the commands dependencyTree, dependencyBrowseGraph
or dependencyBrowseTree can be used to see all dependencies in a detailed way.

### Run and Test

#### Prerequisites to run
For running the application you will need to have an scala full

* JDK 1.8+ installed
* SBT for compile, test, coverage and building
* Scala 2.12+ (for development and build)

#### Run
1. Clone the repository 
    ```bash
    git clone https://david93111@bitbucket.org/david93111/akkaauth.git
    ```
2. go to root of the repository 
3. execute commnand
    ```bash
        sbt reStart
    ```
    This will launch the akka-http with the configured port defined on the application.conf
    inside the main resources folder, default is 
    ```
         http{
            host = "0.0.0.0"
            port = 8012
         }
    ```
4. you can use the Postman API Doc and collection to consume the API:
https://documenter.getpostman.com/view/1567366/RWgryJ71

#### Testing the project
In order to execute the tests SBT is needed as well as Scala in the specified versions on the pre-requisites version.

##### Unit Tests
For launch the unit test sbt task, execute the following command inside the root folder
````bash
sbt test
```` 
##### Unit Tests plus coverage
For coverage verification based on the last test execution, use the following command:
````bash
sbt clean coverage test coverageReport
````
Currently coverage is over 85% , the coverage HTML report can be found on the scoverage-report folder inside 
the scala version folder on target

##### Load Tests
This project include some basic load tests using Gatling Sbt Plugin
to execute this tests the following command can be used:
````bash
sbt gatling:test
````
This command will execute all the simulations on gatling source folder,
currently executes the AuthenticationSimulation with 3 parallel scenarios each of 5k requests corresponding
with the three token generation scenarios, for a total of 15k requests using ramp up injection strategy 
distributed over 5 minutes (Mean requests per second 48 to 50) , the scenarios are:
* Valid credentials and valid user
* Invalid credentials error
* Invalid user error (starts with 'A')

HTML detailed report for each simulation results can be found inside gatling folder on target  

On last execution all requests were successful and no request surpass the 10 seconds maximum limit (due to max random delay) using the 
configured dispatcher on Application.conf, mean response was between 4-5 seconds

### License 
This project is licensed under the MIT License - see the 
[LICENSE](https://bitbucket.org/david93111/akkaauth/src/master/LICENSE) file for details
