
name := "AkkaAuth"

version := "0.0.1"

scalaVersion := "2.12.6"

scalaSource in Gatling := sourceDirectory.value / "gatling" / "scala"

lazy val root = (project in file(".")).
  enablePlugins(JavaServerAppPackaging,GatlingPlugin, BuildInfoPlugin).
  settings(
    buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion),
    buildInfoPackage := "co.com.auth.api"
  ).settings(inConfig(Gatling)(Defaults.testSettings): _*)

// AKKA
val akkaHttpVersion = "10.1.5"
val akkaVersion = "2.5.16"
val akkaDependencies = Seq(
  "com.typesafe.akka" %% "akka-actor",
  "com.typesafe.akka" %% "akka-slf4j"
).map(_ % akkaVersion)

// CIRCE
val circeVersion = "0.10.0"
val circeDependencies = Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-parser"
).map(_ % circeVersion)

val generalDependencies = Seq(
  "org.scalatest"          %%   "scalatest"                      % "3.0.5"          % Test,
  "com.typesafe.akka"      %%   "akka-testkit"                   % akkaVersion      % Test,
  "com.typesafe.akka"      %%   "akka-http-testkit"              % akkaHttpVersion  % Test,
  "io.gatling.highcharts"  %    "gatling-charts-highcharts"      % "3.0.0-RC4"      % "test,it",
  "io.gatling"             %    "gatling-test-framework"         % "3.0.0-RC4"      % "test,it",
  "com.typesafe.akka"      %%   "akka-http"                      % akkaHttpVersion,
  "de.heikoseeberger"      %%   "akka-http-circe"                % "1.22.0",
  "com.beachape"           %%   "enumeratum-circe"               % "1.5.13",
  "ch.qos.logback"         %    "logback-classic"                % "1.2.3",
  "com.typesafe"           %    "config"                         % "1.3.3",
)

parallelExecution in Test:=false

libraryDependencies ++= akkaDependencies ++ circeDependencies ++ generalDependencies

mainClass in Compile := Some("co.com.auth.boot.Boot")

coverageMinimum := 80
coverageFailOnMinimum := true

coverageExcludedPackages := "<empty>;Reverse.*;.*Boot.*"

packageName in Universal := name.value